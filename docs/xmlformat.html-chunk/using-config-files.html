<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>4. 
Using Configuration Files
</title><meta name="generator" content="DocBook XSL Stylesheets V1.69.1"><link rel="start" href="index.html" title="
The xmlformat XML Document Formatter
"><link rel="up" href="index.html" title="
The xmlformat XML Document Formatter
"><link rel="prev" href="doc-processing-model.html" title="3. 
The Document Processing Model
"><link rel="next" href="how-xmlformat-works.html" title="5. 
How xmlformat Works
"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">4. 
Using Configuration Files
</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="doc-processing-model.html">Prev</a> </td><th width="60%" align="center"> </th><td width="20%" align="right"> <a accesskey="n" href="how-xmlformat-works.html">Next</a></td></tr></table><hr></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="using-config-files"></a>4. 
Using Configuration Files
</h2></div></div></div><div class="toc"><dl><dt><span class="sect2"><a href="using-config-files.html#config-file-syntax">4.1. 
Configuration File Syntax
</a></span></dt><dt><span class="sect2"><a href="using-config-files.html#formatting-options">4.2. 
Formatting Options
</a></span></dt></dl></div><p>
An <span><strong class="command">xmlformat</strong></span> configuration file specifies formatting
options to be associated with particular elements in XML documents. For
example, you can format <code class="literal">&lt;itemizedlist&gt;</code> elements
differently than <code class="literal">&lt;orderedlist&gt;</code> elements.
(However, you cannot format <code class="literal">&lt;listitem&gt;</code> elements
differentially depending on the type of list in which they occur.) You
can also specify options for a "pseudo-element" named
<code class="literal">*DEFAULT</code>. These options are applied to any element
for which the options are not specified explicitly.
</p><p>
The following sections describe the general syntax of configuration
files, then discuss the allowable formatting options that can be
assigned to elements.
</p><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="config-file-syntax"></a>4.1. 
Configuration File Syntax
</h3></div></div></div><p>
A configuration file consists of sections. Each section begins with a
line that names one or more elements. (Element names do not include the
"&lt;" and "&gt;" angle brackets.) The element line is followed by
option lines that each name a formatting option and its value. Each
option is applied to every element named on its preceding element line.
</p><p>
Element lines and option lines are distinguished based on leading
whitespace (space or tab characters):
</p><div class="itemizedlist"><ul type="disc"><li><p>
Element lines have no leading whitespace.
</p></li><li><p>
Option lines begin with at least one whitespace character.
</p></li></ul></div><p>
On element lines that name multiple elements, the names should be
separated by spaces or commas. These are legal element lines:
</p><pre class="screen">
para title
para,title
para, title
</pre><p>
On option lines, the option name and value should be separated by
whitespace and/or an equal sign. These are legal option lines:
</p><pre class="screen">
normalize yes
normalize=yes
normalize = yes
</pre><p>
Blank lines are ignored.
</p><p>
Lines that begin "#" as the first non-white character are taken as
comments and ignored. Comments beginning with "#" may also follow the
last element name on an element line or the option value on an option
line.
</p><p>
Example configuration file:
</p><pre class="screen">
para
  format        block
  entry-break   1
  exit-break    1
  normalize     yes
  wrap-length   72

literal replaceable userinput command option emphasis
  format        inline

programlisting
  format        verbatim
</pre><p>
It is not necessary to specify all of an element's options at the same
time. Thus, this configuration file:
</p><pre class="screen">
para, title
  format block
  normalize yes
title
  wrap-length 50
para
  wrap-length 72
</pre><p>
Is equivalent to this configuration file:
</p><pre class="screen">
para
  format block
  normalize yes
  wrap-length 72
title
  format block
  normalize yes
  wrap-length 50
</pre><p>
If an option is specified multiple times for an element, the last value
is used. For the following configuration file, <code class="literal">para</code>
ends up with a <code class="literal">wrap-length</code> value of 68:
</p><pre class="screen">
para
  format        block
  wrap-length   60
  wrap-length   72
para
  wrap-length   68
</pre><p>
To continue an element line onto the next line, end it with a backslash
character. <span><strong class="command">xmlformat</strong></span> will interpret the next line as
containing more element names for the current section:
</p><pre class="screen">
chapter appendix article \
section simplesection \
sect1 sect2 sect3 \
sect4 sect5
  format        block
  entry-break   1
  element-break 2
  exit-break    1
  normalize     no
  subindent     0
</pre><p>
Continuation can be useful when you want to apply a set of formatting
options to a large number of elements. Continuation lines are allowed to
begin with whitespace (though it's possible they may appear to the
casual observer to be option lines if they do).
</p><p>
Continuation is not allowed for option lines.
</p><p>
A configuration file may contain options for two special
"pseudo-element" names: <code class="literal">*DOCUMENT</code> and
<code class="literal">*DEFAULT</code>. (The names begin with a "*" character so as
not to conflict with valid element names.)
</p><p>
<code class="literal">*DEFAULT</code> options apply to any element that appears in
the input document but that was not configured explicitly in the
configuration file.
</p><p>
<code class="literal">*DOCUMENT</code> options are used primarily to control line
breaking between top-level nodes of the document, such as the XML
declaration, the <code class="literal">DOCTYPE</code> declaration, the root
element, and any comments or processing instructions that occur outside
the root element.
</p><p>
It's common to supply <code class="literal">*DEFAULT</code> options in a
configuration file to override the built-in values. However, it's
normally best to leave the <code class="literal">*DOCUMENT</code> options alone,
except possibly to change the <code class="literal">element-break</code> value.
</p><p>
Before reading the input document, <span><strong class="command">xmlformat</strong></span> sets up
formatting options as follows:
</p><div class="orderedlist"><ol type="1"><li><p>
It initializes the built-in <code class="literal">*DOCUMENT</code> and
<code class="literal">*DEFAULT</code> options,
</p></li><li><p>
It reads the contents of the configuration file, assigning formatting
options to elements as listed in the file.
</p><p>
Note that although <code class="literal">*DOCUMENT</code> and
<code class="literal">*DEFAULT</code> have built-in default values, the defaults
they may be overridden in the configuration file.
</p></li><li><p>
After reading the configuration file, any missing formatting options for
each element are filled in using the options from the
<code class="literal">*DEFAULT</code> pseudo-element. For example, if
<code class="literal">para</code> is defined as a block element but no
<code class="literal">subindent</code> value is defined, <code class="literal">para</code>
"inherits" the <code class="literal">subindent</code> value from the
<code class="literal">*DEFAULT</code> settings.
</p></li></ol></div><p>
Missing options are filled in from the <code class="literal">*DEFAULT</code>
options only <span class="emphasis"><em>after</em></span> reading the entire configuration
file. For the settings below, <code class="literal">*DEFAULT</code> has a
<code class="literal">subindent</code> value of 2 (not 0) after the file has been
read. Thus, <code class="literal">para</code> also is assigned a
<code class="literal">subindent</code> value of 2.
</p><pre class="screen">
*DEFAULT
  subindent 0
para
  format block
  normalize yes
*DEFAULT
  subindent 2
</pre></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="formatting-options"></a>4.2. 
Formatting Options
</h3></div></div></div><p>
The allowable formatting options are as follows:
</p><pre class="screen">
format {block | inline | verbatim}
entry-break <em class="replaceable"><code>n</code></em>
element-break <em class="replaceable"><code>n</code></em>
exit-break <em class="replaceable"><code>n</code></em>
subindent <em class="replaceable"><code>n</code></em>
normalize {no | yes}
wrap-length <em class="replaceable"><code>n</code></em>
</pre><p>
A value list shown as <code class="literal">{ value1 | value2 | ... }</code>
indicates that the option must take one of the values in the list. A
value shown as <em class="replaceable"><code>n</code></em> indicates that the option
must have a numeric value.
</p><p>
Details for each of the formatting options follow.
</p><div class="itemizedlist"><ul type="disc"><li><p>
<code class="literal">format {block | inline | verbatim}</code>
</p><p>
This option is the most important, because it determines the general way
in which the element is formatted, and it determines whether the other
formatting options are used or ignored:
</p><div class="itemizedlist"><ul type="circle"><li><p>
For block elements, all other formatting options are significant.
</p></li><li><p>
For inline elements, all other formatting options are ignored. Inline
elements are normalized, wrapped, and indented according to the
formatting options of the enclosing block element.
</p></li><li><p>
For verbatim elements, all other formatting options are ignored. The
element content is written out verbatim (literally), without change,
even if it contains other sub-elements. This means no normalization of
the contents, no indenting, and no line-wrapping. Nor are any breaks
added within the element.
</p></li></ul></div><p>
A configuration file may <span class="emphasis"><em>specify</em></span> any option for
elements of any type, but <span><strong class="command">xmlformat</strong></span> will ignore
inapplicable options. One reason for this is to allow you to experiment
with changing an element's format type without having to disable other
options.
</p><p>
If you use the <code class="option">--show-config</code> command-line option to see
the configuration that <span><strong class="command">xmlformat</strong></span> will use for
processing a document, it displays only the applicable options for each
element.
</p></li><li><p>
<code class="literal">entry-break <em class="replaceable"><code>n</code></em></code>
</p><p>
<code class="literal">element-break <em class="replaceable"><code>n</code></em></code>
</p><p>
<code class="literal">exit-break <em class="replaceable"><code>n</code></em></code>
</p><p>
These options indicate the number of newlines (line breaks) to write
after the element opening tag, between child sub-elements, and before
the element closing tag. They apply only to block elements.
</p><p>
A value of 0 means "no break". A value of 1 means one newline, which
causes the next thing to appear on the next line with no intervening
blank line. A value <em class="replaceable"><code>n</code></em> greater than 1 produces
<em class="replaceable"><code>n</code></em>-1 intervening blank lines. Some examples:
</p><div class="itemizedlist"><ul type="circle"><li><p>
An <code class="literal">entry-break</code> value of 0 means the next token will
appear on same line immediately after the opening tag.
</p></li><li><p>
An <code class="literal">exit-break</code> value of 0 means the closing tag will
appear on same line immediately after the preceding token.
</p></li></ul></div></li><li><p>
<code class="literal">subindent <em class="replaceable"><code>n</code></em></code>
</p><p>
This option indicates the number of spaces by which to indent child
sub-elements, relative to the indent of the enclosing parent. It applies
only to block elements. The value may be 0 to suppress indenting, or a
number <em class="replaceable"><code>n</code></em> greater than 0 to produce indenting.
</p><p>
This option does not affect the indenting of the element itself. That is
determined by the <code class="literal">subindent</code> value of the element's
own parent.
</p><p>
Note: <code class="literal">subindent</code> does not apply to text nodes in
non-normalized blocks, which are written as is without reformatting.
<code class="literal">subindent</code> also does not apply to verbatim elements or
to the following non-element constructs, all of which are written with
no indent:
</p><div class="itemizedlist"><ul type="circle"><li><p>
Processing instructions
</p></li><li><p>
Comments
</p></li><li><p>
<code class="literal">DOCTYPE</code> declarations
</p></li><li><p>
<code class="literal">CDATA</code> sections
</p></li></ul></div></li><li><p>
<code class="literal">normalize {no | yes}</code>
</p><p>
This option indicates whether or not to perform whitespace normalization
in text. This option is used for block elements, but it also affects
inline elements because their content is normalized the same way as
their enclosing block element.
</p><p>
If the value is <code class="literal">no</code>, whitespace-only text nodes are
not considered significant and are discarded, possibly to be replaced
with line breaks and indentation.
</p><p>
If the value is <code class="literal">yes</code>, normalization causes removal of
leading and trailing whitespace within the element, and conversion of
runs of whitespace characters (including line-ending characters) to
single spaces.
</p><p>
Text normalization is discussed in more detail in
<a href="doc-processing-model.html#text-handling" title="3.3. 
Text Handling
">Section 3.3, “
Text Handling
”</a>.
</p></li><li><p>
<code class="literal">wrap-length <em class="replaceable"><code>n</code></em></code>
</p><p>
Line-wrapping length. This option is used only for block elements and
line-wrapping occurs only if normalization is enabled. The option
affects inline elements because they are line-wrapped the same way as
their enclosing block element.
</p><p>
Setting the <code class="literal">wrap-length</code> option to 0 disables
wrapping. Setting it to a value <em class="replaceable"><code>n</code></em> greater
than 0 enables wrapping to lines at most <em class="replaceable"><code>n</code></em>
characters long. (Exception: If a word longer than
<em class="replaceable"><code>n</code></em> characters occurs in text to be wrapped, it
is placed on a line by itself. A word will never be broken into pieces.)
The line length is adjusted by the current indent when wrapping is
performed to keep the right margin of wrapped text constant. For example
if the <code class="literal">wrap-length</code> value is 60 and the current indent
is 10, lines are wrapped to a maximum of 50 characters.
</p><p>
Any prevailing indent is added to the beginning of each line, unless the
text will be written immediately following a tag on the same line. This
can occur if the text occurs after the opening tag of the block and the
<code class="literal">entry-break</code> is 0, or the text occurs after the
closing tag of a sub-element and the <code class="literal">element-break</code> is
0.
</p></li></ul></div></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="doc-processing-model.html">Prev</a> </td><td width="20%" align="center"> </td><td width="40%" align="right"> <a accesskey="n" href="how-xmlformat-works.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">3. 
The Document Processing Model
 </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top"> 5. 
How <span><strong class="command">xmlformat</strong></span> Works
</td></tr></table></div></body></html>
