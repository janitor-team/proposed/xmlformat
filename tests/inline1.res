<!--
Test to show that normalization of inline elements is done the
same way as the enclosing block.
-->

<para-norm>This is a normalized paragraph <inline> with an inline
</inline> element.</para-norm>

<para-no-norm>
This is a non-normalized paragraph
<inline>   with an inline   </inline>
element.
</para-no-norm>
