<!--
The para element is configured with entry-break and element-break set to 0.
This means that indent of text should be suppressed for lines that occur
immediately after the opening para tag, or after the closing tag of any
para sub-element.
-->
<para>This is a line of text in the outer
  para. This is a line of text in the
  outer para. This is a line of text in
  the outer para. This is a line of text
  in the outer para.<para>This is a line of text in the inner
    para. This is a line of text in the
    inner para. This is a line of text
    in the inner para. This is a line of
    text in the inner para.</para>This is a line of text in the inner
  para. This is a line of text in the
  inner para. This is a line of text in
  the inner para. This is a line of text
  in the inner para.</para>
